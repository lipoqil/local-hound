# Local Hound

This is small helper for running [Hound search](https://github.com/hound-search/hound) locally

## Run it

1. Generate config for you projects -- `PROJECTS_HOME=~/Projects/my ./refresh-repos.zsh > config.json`
2. Start Hound search -- `PROJECTS_HOME=~/Projects/my make run`
    - This will start pretty much instantly, but it will take a relatively long time to index all the repos