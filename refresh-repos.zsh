#!/usr/bin/env zsh

# This script generates Hound search config -- https://github.com/hound-search/hound/blob/master/config-example.json
#
# It needs folowing variables
#   PROJECTS_HOME : Where projects to be indexed, lies
#
# TODO: Last item repos key should not have trailing comma
# TODO: The dirs and files paths should be taken from variable

new_config_path='new-config.json'

cat << CONFIG_HEADER
  {
    "" : "Taking projects from ${PROJECTS_HOME}",
    "" : "Running in zsh ${ZSH_VERSION}",
    "max-concurrent-indexers" : 2,
    "dbpath" : "data",
    "health-check-uri" : "/healthz",
    "repos" : {
CONFIG_HEADER

for repo_data_path in ${PROJECTS_HOME}/**/.git(e:'[[ ! $REPLY =~ ".*local-hound/data/vcs.*"  ]]':) ; do
  repo_path=(${repo_data_path:h})
  cat << REPOSITEM
        "${repo_path:t}" : {
            "url" : "file://${repo_path:s_/Users/mailo/Projects/my_/projects_}"
        },
REPOSITEM
done

echo '    }
}'
